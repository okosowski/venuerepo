# Hello!


### this is one of multiple way how to use angular with es6, googleMaps and all build in webpack

---
#### what should be done
> create a website with Google Maps on the left and with the list of spaces on the right

Done
> upload the list by creating request via Ajax to back-end: http://vsapi.ragnak.com/spaces?location=London,%20UK&center=51.5073509,-0.12775829999998223&bounds=51.46127765398595,-0.354007994824201,51.55337760399641,0.09849139482423652&page=1&is_mobile=null

Done
> the miniatures of spaces should appear once data is fetched from api - display pictures and names on right side list. the localization of them should be visible on the map as pins

Done

> there should be a pop up with miniature and the name of space after placing the cursor on the pin, the space should be highlighted on the list

Done

> url to api above returns not only spaces but also the number of spaces and results. The pagination can be created by using this information. When the page will change the parameter 'page' should be send to the method in order to download the results for specific page. In this case the map and the list should be cleaned and new spaces should be downloaded

Done

---
# Also !!!
- all project I based on Angular,not only jQuery (Mihaela advised me to do this :) ) 
- **native ES6** (promise and stuff like this) in venueService, in future we can simple  export this part of code to separate project ond import it by mpn
- **cache** - max count of request witch web application can do is equal to number of site with data. For example if the user returns to the page where he was, data for this site we get from cache (__venueService -> this.data[page]__) not from the vsApi. This can reduce the number of queries to the backend!
- usually 30% of image request return 404 Error, in this case we replace original (failed) image, another **no-imamge** picture 
- I did't have enough time to study **ui-gmap-google-map** documentation  (it seems very cool) and I assume that this can be done more efficiently (like use ui-gmap-markers over ui-gmap-marker)
- I add same kind of **block User Interface**, only on request promise
- I add **search** input wich simply filer result from list and map
- and more ...
---
# api Error
all for request like
> GET http://vsapi.ragnak.com/spaces?location=London,%20UK&center=51.5073509,-0.12775829999998223&bounds=51.46127765398595,-0.354007994824201,51.55337760399641,0.09849139482423652&page=1&is_mobile=null

independently from the **&page=** returns results probably identical :/

for example first three spaces for page 1,2,3

|space|**&page=1**|**&page=2**|**&page=3**|
| ------------- |:-------------:|:-----:|:-----:|
| req.data.spaces 0 _id | 56ab89694c156d3f0968f394 |56ab89694c156d3f0968f394 |56ab89694c156d3f0968f394|
| req.data.spaces 1 _id |56ab89574c156d3f0968f392|56ab89574c156d3f0968f392|56ab89574c156d3f0968f392|
| req.data.spaces 2 _id |56ab89614c156d3f0968f393|56ab89614c156d3f0968f393|56ab89614c156d3f0968f393|

**I assume that these results for different page should be different :)**


---
#### git repo
https://bitbucket.org/okosowski/venuerepo/

#### to download repo
```shell
cd go/to/dir
git clone https://bitbucket.org/okosowski/venuerepo.git
```

#### to download all npm stuff
```shell
npm install
```

##### start webserver
```shell
npm start
```

##### to create compiled file
```shell
webpack
```
if you don't have install webpack globally (npm install -g webpack), propably you shoud enter smt like
```shell
./node_modules/.bin/webpack
```


---
### or simply see **DEMO**
on http://www.oskarkosowski.com/venue


