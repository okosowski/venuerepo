export function prepareRequest(url, method = 'GET') {
  let xhr = new XMLHttpRequest()
  xhr.open(method, url)
  xhr.setRequestHeader('Accept', 'application/json')
  return xhr
}

/**
 * Funkcja dodająca obsugę błędów dla zadanego obiektu XHR.
 * @param xhr XMLHttpRequest
 * @param callback Function
 */
export function addErrorHandlers(xhr, callback) {
  ['error', 'abort', 'timeout'].forEach((error) => {
    xhr.addEventListener(error, callback)
  })
}
