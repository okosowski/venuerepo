import { prepareRequest, addErrorHandlers } from './request'
import { makeResponse } from './response'

import each from '../../node_modules/lodash/each'
import isEmpty from '../../node_modules/lodash/isEmpty'

let urlMock = {
  protocol: 'http://',
  host: 'vsapi.ragnak.com',
  pathname: '/spaces',
  search: {
    'location': 'London,%20UK',
    'center' : [51.5073509, -0.12775829999998223],
    'bounds' : [51.46127765398595, -0.354007994824201, 51.55337760399641, 0.09849139482423652],
    'page' : 1,
    'is_mobile' : null
  }
}

export default class venueData {
  constructor($http) {
    this.baseUrl = this.createMockUrl()
    this.data = {}
    this.spaces = []
  }

  reset(){
    this.baseUrl = this.createMockUrl()
    this.data = {}
    this.spaces = []
  }

  createMockUrl(page, isMobile = null) {
    if (!page){return false}
    let url = urlMock.protocol + urlMock.host + urlMock.pathname
    let location = urlMock.search.location ? '?location=' + urlMock.search.location : ''
    let center = urlMock.search.center ? '&center=' + urlMock.search.center.toString() : ''
    let bounds = urlMock.search.bounds ? '&bounds=' + urlMock.search.bounds.toString() : ''
    let pageCount = '&page=' + page.toString()
    let mobile = '&is_mobile=' + isMobile
    return url + location + center + bounds + pageCount + mobile
  }

  fetch(params = {page: 1}) {
    let promise = new Promise((resolve, reject) => {
      let xhr = prepareRequest(this.createMockUrl(params.page))
      xhr.addEventListener('load', () => {
        let response = makeResponse(xhr)
        return response.success ? resolve(response) : reject(response)
      })
      addErrorHandlers(xhr, () => { reject(makeResponse(xhr)) })
      xhr.send()
    })
      .then((res)=>{
        this.convertResponse(res, params.page)
        return Promise.resolve(res)
      })
      .catch((e) => { return Promise.reject(e) })

    return promise
  }

  convertResponse(response, page){
    if(!!response.success){
      this.data[page] = JSON.parse(response.response).data
      this.data.currentSiteNo = page
      this.data.allSitesNumber = this.data[page].pages
    }else{
      console.error('error while convertResponse')
    }
  }

  getCenterCoordinates(){
    return {latitude: urlMock.search.center[0].toFixed(3), longitude: urlMock.search.center[1].toFixed(3)}
  }
  getCurrentSiteNo(){
    return this.data.currentSiteNo
  }
  setCurrentSiteNo(number){
    if (!number){return false}
    this.data.currentSiteNo = number
    return this.data.currentSiteNo
  }
  getAllSitesNumber(){
    return this.data.allSitesNumber
  }
}



