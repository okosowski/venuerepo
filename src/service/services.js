import angular from 'angular';
import venueService from './venueService';

export default angular
  .module('venueApp.services', [])
  .service({
    venueService
  });
