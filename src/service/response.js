export function makeResponse(xhr) {
  let response = {
    status: xhr.status,
    response: xhr.responseText,
    success: xhr.status >= 200 && xhr.status < 300,
    xhr: xhr
  }
  try {
    let json = JSON.parse(xhr.response)
    response._meta = json._meta
    if (json.data || json) {
      response.data = json.data || json
    }
  } catch (e){}

  return response
}
