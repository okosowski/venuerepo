import angular from 'angular'
import ngRoute from 'angular-route'

import services from './service/services'
import views from './views/views'

import './styles.scss'

import lodash from 'lodash'
import 'angular-google-maps'


angular
    .module('venueApp',
        [ngRoute, services.name, views.name]
    )
    .config(['$routeProvider',
      function($urlRouterProvider) {
        $urlRouterProvider.otherwise({redirectTo: '/'})
      }
    ])