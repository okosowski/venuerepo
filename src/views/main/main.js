import angular from 'angular'
import ngRounte from 'angular-route'
import template from './main.html'

import './main.scss'

let mainViewModule = angular
  .module('venueApp.main', [ngRounte, 'uiGmapgoogle-maps'])
  .config(['$routeProvider', 'uiGmapGoogleMapApiProvider', function($routeProvider, uiGmapGoogleMapApiProvider) {
    $routeProvider.when('/', {
      template: template,
      controller: 'mainViewCtrl',
      controllerAs: 'vm'
    })
    uiGmapGoogleMapApiProvider.configure({
      key: 'AIzaSyD6mSIg7bdp6JCuOZ3JnFuq72YlWDwas2w',
      //v: '3.20', //defaults to latest 3.X anyhow
      libraries: 'weather,geometry,visualization'
    })
  }])
  .controller('mainViewCtrl', mainViewCtrl)
  .directive('errSrc', function() {
    return {
      link: function(scope, element, attrs) {
        scope.$watch(function() {
          return attrs['ngSrc'];
        }, function (value) {
          if (!value) {
            element.attr('src', attrs.errSrc);
          }
        });
        element.bind('error', function() {
          element.attr('src', attrs.errSrc);
        });
      }
    }
  });


function mainViewCtrl(venueService, $scope, uiGmapGoogleMapApi){
  let vm = this
  vm.isLoading = true
  vm.data = {}
  vm.data.centerCoordinate = venueService.getCenterCoordinates()
  venueService.fetch({page: 1})
    .then((result)=>{
      vm.data.spaces = venueService.data[1].spaces
      vm.data.currentSiteNo = venueService.getCurrentSiteNo()
      vm.data.allSitesNumber = venueService.getAllSitesNumber()
      vm.isLoading = false
      $scope.$apply()
    })
    .catch((error)=>{
      console.error(error)
      vm.isLoading = false
    })


  vm.getSite = (siteNumber) => {
    vm.isLoading = true
    let params = {}
    if (!angular.isDefined(siteNumber)) {
      siteNumber = 1
    }

    if (venueService.data[siteNumber]){
      venueService.setCurrentSiteNo(siteNumber)
      vm.data.spaces = venueService.data[siteNumber].spaces
      vm.data.currentSiteNo = venueService.getCurrentSiteNo()
      vm.data.allSitesNumber = venueService.getAllSitesNumber()
      vm.isLoading = false
      //$scope.$apply()
    }else{
      venueService.fetch({page: siteNumber})
        .then((result)=>{
          vm.data.spaces = venueService.data[siteNumber].spaces
          vm.data.currentSiteNo = venueService.getCurrentSiteNo()
          vm.data.allSitesNumber = venueService.getAllSitesNumber()
          vm.isLoading = false
          $scope.$apply()
        })
        .catch((error)=>{
          console.error(error)
          vm.isLoading = false
        })
    }
  }

  let onMarkerClicked = (marker) => {
    marker.showWindow = true
  };


  vm.map = {
    doCluster: true,
    options: {
      streetViewControl: false,
      panControl: false,
      maxZoom: 18,
      minZoom: 3
    },
    events: {},
    center: venueService.getCenterCoordinates(),
    clusterOptions: {},
    zoom: 10,
    marker: {
      events: {
        click: function(gMarker, eventName, model) {
          onMarkerClicked(gMarker)
        }
      }
    }
  };

  vm.clickMarker = (id) =>{
    console.log('click marker', id)
  }

  return vm
}
mainViewCtrl.$inject = ['venueService', '$scope', 'uiGmapGoogleMapApi']

export default mainViewModule