import angular from 'angular';
import mainViewModule from './main/main';

let views = angular.module('venueApp.views', [
  mainViewModule.name
])

export default views